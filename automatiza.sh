#!/bin/bash

################################PARAMETROS######################################

DIR_SRC="/home/carloscichon/LabMeteorologia/PyCPT/dados/ecmwf/BaixaDados/src/"
DIR_BIN="/home/carloscichon/LabMeteorologia/PyCPT/dados/ecmwf/BaixaDados/bin/"

##### #Gera Datas (o dia deve ser uma segunda-feira) #######
DIA_BASE=1
MES_BASE=1
ANO_BASE=2018
ANOF_BASE=2018 #ainda não funciona com um ano diferente do inicial

LISTA_ANOS="1999 2000 2001" #anos dos hindcasts

DIAS_ANTECEDENCIA=10 #dias de antecedencia da previsão

ARQ_SAIDA="ecmwf_10dias"

##################################################################################

if [[ ! -d saida ]]; then
	mkdir saida
fi

if [[ ! -d dados ]]; then
	mkdir dados
fi

if [[ ! -d parametros ]]; then
	mkdir parametros
fi


gcc ${DIR_SRC}/geraDatas.c -o ${DIR_BIN}/geraDatas
${DIR_BIN}/geraDatas ${DIA_BASE} ${MES_BASE} ${ANO_BASE} ${ANOF_BASE} > datas_tmp.txt

PREVI=$(echo $(($DIAS_ANTECEDENCIA * 24)))

PREVF=$(echo $(($DIAS_ANTECEDENCIA + 3)))
PREVF=$(echo $(($PREVF * 24)))

#echo $PREVI
#echo $PREVF

while read linha; do
	for ano in $LISTA_ANOS; do
		cp param_padrao.py parametros/${linha}-${ano}.py
		echo DIA ${ano}-${linha}
		sed -i "s#<DATE>#${ANOF_BASE}-${linha}#g;
			s#<HDATE>#${ano}-${linha}#g;
			s#<SAIDA>#dados/dia_${linha}-${ano}.nc#g
			s#<PREVI>#${PREVI}#g
			s#<PREVF>#${PREVF}#g
		" parametros/${linha}-${ano}.py
		python parametros/${linha}-${ano}.py
	done
done < datas_tmp.txt

cdo -b F64 mergetime dados/*.nc saida/temp.nc
cdo daymean saida/temp.nc saida/${ARQ_SAIDA}.nc
rm saida/temp.nc