#!/usr/bin/env python
from ecmwfapi import ECMWFDataServer
server = ECMWFDataServer()
server.retrieve({
    "class": "s2",
    "dataset": "s2s",
    "date": "<DATE>",
    "expver": "prod",
    "hdate": "<HDATE>",
    "levtype": "sfc",
    "model": "glob",
    "origin": "ecmf",
    "param": "228228",
    "step": "<PREVI>/to/<PREVF>/by/24",
    "stream": "enfh",
    "time": "00:00:00",
    "type": "cf",
    "format": "netcdf",
    "target": "<SAIDA>",
})
