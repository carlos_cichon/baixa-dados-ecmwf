#include <stdio.h>
#include <stdlib.h>

#define MAX 2


int main(int *argc, char *argv[]){
	int dayI, monthI, yearI, yearF;
	int months[12] = {31,28,31,30,31,30,31,31,30,31,30,31};
	int flag = 1;

	dayI = atoi(argv[1]);
	monthI = atoi(argv[2]);
	yearI = atoi(argv[3]);
	yearF = atoi(argv[4]);

	for (int i = atoi(argv[3]); i <= yearF; i++){
		while(monthI <= 12){
			while(dayI <= months[monthI-1]){
				printf("%d-%02d-%02d/",yearI,monthI,dayI);

				if (flag){
					dayI = dayI + 3;
					flag=0;
				}
				else{
					dayI = dayI + 4;
					flag=1;
				}
			}

			dayI=dayI-months[monthI-1];
			monthI++;
			
		}
		monthI=1;
		yearI++;
		
	}

}